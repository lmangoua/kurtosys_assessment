/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kurtosysassessment;

import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author Lionel Mangoua
 * Date: 21/09/17
 * Description: Functional UI Test
 */
public class KurtosysAssessment {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        
        //Set Property of ChromeDriver
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\C55-C1881 White\\Documents\\Softwares\\SeleniumDrivers\\chromedriver_win32\\chromedriver.exe");
        
        //Setup Driver
        WebDriver driver = new ChromeDriver();
        
        //Open URL
        driver.get("https://am.jpmorgan.com/us/en/asset-management/gim/liq/home");
        
        //Maximize window
        driver.manage().window().maximize();
        
        //Accept Disclaimer
        WebElement frame = driver.findElement(By.xpath("//div[@class='modal-content']"));
        driver.switchTo().frame(frame);
        
        WebElement accept;
        
        accept = driver.findElement(By.className("btn btn-cta ng-binding"));
        accept.click();
        
        driver.switchTo().defaultContent();
                
        //Wait for Find a Fund
        
        Thread.sleep(5000);
        
        //Click View All
        WebElement viewAll;
        
        viewAll = driver.findElement(By.className("minife3__btn minife3__btn--ghost"));
        viewAll.click();
        
        Thread.sleep(5000);
        
        //Click A
        WebElement checkboxA;
        
        checkboxA = driver.findElement(By.id("f-A"));
        checkboxA.click();
        
        Thread.sleep(5000);
        
        //Verify A is checked
        boolean enabled = driver.findElement(By.id("f-A")).isEnabled();

        if (!enabled) {
            System.out.println("Already checked"); 
        } 
        else { 
            driver.findElement(By.id("f-A")).click(); 
        }
        
        //Enter value in search bar
        WebElement searchValue;
        
        searchValue = driver.findElement(By.className("fe3__search--input ng-pristine ng-valid"));
        
        searchValue.sendKeys("48121A407");
        
        Thread.sleep(5000);
        
        //Click Search
        WebElement searchBtn;
        
        searchBtn = driver.findElement(By.className("ng-binding"));
        searchBtn.click();
        
        Thread.sleep(5000);
        
        //Click fund
        WebElement fund;
        
        fund = driver.findElement(By.className("dropdownleftName ng-scope ng-binding"));
        fund.click();
        
        Thread.sleep(5000);
        
        //click Portfolio tab
        WebElement portfolioTb;
        
        portfolioTb = driver.findElement(By.className("ng-isolate-scope"));
        portfolioTb.click();
        
        Thread.sleep(5000);
        
        //Verify chart exist        
        WebElement chart;
        
        chart = driver.findElement(By.className("highcharts-background"));
        Assert.assertEquals(true, chart.isDisplayed());


        
    }
    
}
